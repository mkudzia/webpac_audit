# webpacaudit.py

import os
import csv

webpac_dir = '/path/to/screens/backup/'
csv_file = '/path/to/csv.csv'


class WebPac(object):
    __slots__ = ('file_name', 'file_type', 'active_status', 'comments')
    _rowtypes = (str, str, str, str)

    def __init__(self, file_name, file_type, active_status, comments):
        self.file_name = file_name
        self.file_type = file_type
        self.active_status = active_status
        self.comments = comments
        self.columns = {
            "File name": [],  # do I need this one?
            "File type": [],
            "Active status": [],
            "Comments": [],
        }

        def get_file_info(dir):
            for filenames in dir:
                from os import path
                print(path)
                filename = os.path.split(dir)
                filename = os.path.splitext(filename)[0]
                filetype = os.path.splitext(filename)[1]
                self.columns["File name"].append(filename)
                self.columns["File type"].append(filetype)
                self.columns["Active status"].append(" ")
                self.columns["Comments"].append(" ")

        def write_file_info(csvfile):
            with open(csvfile, 'w') as the_file:
                fieldnames = ['File name', 'File type', 'Active status', 'Comments']
                audit_writer = csv.DictWriter(the_file, fieldnames=fieldnames)

                audit_writer.writeheader()
                audit_writer.writerow([self.columns[0]])  # or self.columns(filename) ?
                audit_writer.writerow([self.columns[1]])
                audit_writer.writerow([self.columns[2]])
                audit_writer.writerow([self.columns[3]])
